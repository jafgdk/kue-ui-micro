var express = require('express');
var app = express();

var kue = require('kue');
var ui = require('kue-ui');

app.get('/', function(req, res, next){
  res.json({success: true});
});

ui.setup({
  apiURL: '/api',
  baseURL: '/test',
  updateInterval: 1000
});

app.use('/api', kue.app);
app.use('/test', ui.app);

module.exports = app;
